package test;

import model.Rechteck;
import model.Punkt;
import controller.BunteRechteckeController;

public class RechteckTest {

	public static void main(String[] args) {

		// Parameterlose Rechtecke generieren
		Rechteck re0 = new Rechteck();
		Rechteck re1 = new Rechteck();
		Rechteck re2 = new Rechteck();
		Rechteck re3 = new Rechteck();
		Rechteck re4 = new Rechteck();

		// Parameterlose Rechtecke bef�llen
		re0.setX(10);
		re0.setY(10);
		re0.setBreite(30);
		re0.setHoehe(40);

		re1.setX(25);
		re1.setY(25);
		re1.setBreite(100);
		re1.setHoehe(20);

		re2.setX(260);
		re2.setY(10);
		re2.setBreite(200);
		re2.setHoehe(100);

		re3.setX(5);
		re3.setY(500);
		re3.setBreite(300);
		re3.setHoehe(25);

		re4.setX(100);
		re4.setY(100);
		re4.setBreite(100);
		re4.setHoehe(100);

		// Rechtecke mit Parametern generieren
		Rechteck re5 = new Rechteck(new model.Punkt(200, 200), 200, 200);
		Rechteck re6 = new Rechteck(new model.Punkt(800, 400), 20, 20);
		Rechteck re7 = new Rechteck(new model.Punkt(800, 450), 20, 20);
		Rechteck re8 = new Rechteck(new model.Punkt(850, 400), 20, 20);
		Rechteck re9 = new Rechteck(new model.Punkt(855, 455), 25, 25);

		// Vergleich der Rechteck toString Methode mit einem String
		String zeichenkette = "Rechteck [x=10, y=10, breite=30, hoehe=40]";
		System.out.println(re0.toString());
		System.out.println(re0.toString().equals(zeichenkette));
		System.out.println();

		// Erstellen und bef�llen eines Controllers
		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(re0);
		controller.add(re1);
		controller.add(re2);
		controller.add(re3);
		controller.add(re4);
		controller.add(re5);
		controller.add(re6);
		controller.add(re7);
		controller.add(re8);
		controller.add(re9);

		// Ausgabe des Controllers
		String zeichenkette2 = "BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=30, hoehe=40], Rechteck [x=25, y=25, breite=100, hoehe=20], Rechteck [x=260, y=10, breite=200, hoehe=100], Rechteck [x=5, y=500, breite=300, hoehe=25], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]";
		System.out.println(controller.toString());
		System.out.println(controller.toString().equals(zeichenkette2));
		System.out.println();

		// Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glich ist
		Rechteck eck10 = new Rechteck(new model.Punkt(-4, -5), -50, -200);
		System.out.println(eck10); // Rechteck [x=-4, y=-5, breite=50, hoehe=200]
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);// Rechteck [x=-10, y=-10, breite=200, hoehe=100]

		// Random Rechteck testen
		Rechteck eck12 = Rechteck.generiereZufallsRechteck();
		Rechteck eck13 = Rechteck.generiereZufallsRechteck();
		Rechteck eck14 = Rechteck.generiereZufallsRechteck();
		System.out.println("");
		System.out.println(eck12.toString());
		System.out.println(eck13.toString());
		System.out.println(eck14.toString());
		System.out.println("");

		System.out.println(Rechteck.rechteckeTesten());
	}

}
