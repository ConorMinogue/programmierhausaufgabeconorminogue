package model;

import java.util.Random;

public class Rechteck {
	private Punkt p;
	private int breite;
	private int hoehe;
	Random rand = new Random();

	public Rechteck() {
		super();
		this.p = new Punkt();
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(Punkt p, int breite, int hoehe) {
		super();
		this.p = p;
		this.breite = absolute(breite);
		this.hoehe = absolute(hoehe);
	}

	public int getX() {
		return p.getX();
	}

	public void setX(int x) {
		p.setX(x);
	}

	public int getY() {
		return p.getY();
	}

	public void setY(int y) {
		p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = absolute(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = absolute(hoehe);
	}

	public int absolute(int zahl) {
		return Math.abs(zahl);
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

	public boolean enthaelt(int x, int y) {
		// Pr�fen, ob die x Koordinate im Bereich des Rechtecks liegt
		if ((x <= (this.p.getX() + this.getBreite())) && (x >= this.p.getX())) {
			// Pr�fen, ob die y Koordinate im Bereich des Rechtecks liegt
			if ((y <= (this.p.getY() + this.getHoehe())) && (y >= this.p.getY())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public boolean enthaelt(Punkt p) {
		// Pr�fen, ob die X Koordinate vom Parameter p im Bereich des Rechtecks liegt
		if ((p.getX() <= (this.p.getX() + this.getBreite())) && (p.getX() >= this.p.getX())) {
			// Pr�fen, ob die Y Koordinate vom Parameter p im Bereich des Rechtecks liegt
			if ((p.getY() <= (this.p.getY() + this.getHoehe())) && (p.getY() >= this.p.getY())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public boolean enthaelt(Rechteck rechteck) {
		if ((enthaelt(rechteck.getX(), rechteck.getY()) == true) && (rechteck.getBreite() < this.getBreite())
				&& (rechteck.getHoehe() < this.getHoehe())) {
			return true;
		} else {
			return false;
		}

	}

	public static Rechteck generiereZufallsRechteck() {
		Random rand = new Random();
		int tx = rand.nextInt(1200);
		int ty = rand.nextInt(1000);

		int tbreite = rand.nextInt(1200);
		while ((tx + tbreite) >= 1200) {
			tbreite--;
		}

		int thoehe = rand.nextInt(1000);
		while ((ty + thoehe) >= 1000) {
			thoehe--;
		}

		return new Rechteck(new Punkt(tx, ty), tbreite, thoehe);
	}

	public static boolean rechteckeTesten() {
		boolean tester = false;
		Rechteck grundlage = new Rechteck(new Punkt(0, 0), 1200, 1000);
		Rechteck[] rechtecke = new Rechteck[50000];
		for (int i = 0; i < rechtecke.length; i++) {
			rechtecke[i] = Rechteck.generiereZufallsRechteck();
			if (grundlage.enthaelt(rechtecke[i]) == false) {
			} else {
				tester = true;
			}
		}
		return tester;

	}

}
