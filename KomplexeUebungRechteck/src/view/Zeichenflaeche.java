package view;

import controller.BunteRechteckeController;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;
import javax.swing.JPanel;

public class Zeichenflaeche extends JPanel {

	private final BunteRechteckeController CONTROLLER;

	public Zeichenflaeche(BunteRechteckeController controller) {
		super();
		this.CONTROLLER = controller;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 50, 50);

		for (int i = 0; i < CONTROLLER.getRechtecke().size(); i++) {
			g.drawRect(CONTROLLER.getRechtecke().get(i).getX(), CONTROLLER.getRechtecke().get(i).getY(),
					CONTROLLER.getRechtecke().get(i).getBreite(), CONTROLLER.getRechtecke().get(i).getHoehe());
		}
	}
}
