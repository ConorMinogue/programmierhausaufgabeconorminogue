package controller;

import model.Rechteck;
import java.util.LinkedList;

public class BunteRechteckeController {

	private LinkedList<Rechteck> rechtecke;

	public static void main(String[] args) {
	}

	public BunteRechteckeController() {
		super();
		rechtecke = new LinkedList<Rechteck>();
	}

	public void add(Rechteck rechteck) {
		this.rechtecke.add(rechteck);
	}

	public void reset() {
		this.rechtecke.clear();
	}

	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void generiereZufallsRechtecke(int anzahl) {
		reset();
		for (int i = 0; i < anzahl; i++) {
			rechtecke.add(model.Rechteck.generiereZufallsRechteck());
		}
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

}
